package fr.jctarby.lancerdesapplications

import android.content.ActivityNotFoundException
import android.content.Intent
import android.icu.util.Calendar
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import fr.jctarby.lancerdesapplications.ui.theme.LancerDesApplicationsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LancerDesApplicationsTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    MyApp()
                }
            }
        }
    }
}

@Composable
fun MyApp() {
    val contexte = LocalContext.current.applicationContext
    // Créez un lanceur d'activité pour gérer le résultat de l'intention
    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { _ -> }

    Column(modifier = Modifier
            .fillMaxSize()
            .padding(16.dp), verticalArrangement = Arrangement.SpaceEvenly
    )
    {
        Button(
                onClick = {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=fr.univ_lille_1.ieea.jc_tarby.boite_a_meuh"))
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)  // On lance une nouvelle activité depuis un Composable --> New task
                    try {
                        contexte.startActivity(intent)
                    } catch (e: Exception) {
                        Log.d("ERROR", "erreur ${e.message}")
                    }
                },

                ) {
            Text("Ouvrir Boite à Meuh sur le Play Store")
        }
        Button(
                onClick = {
                    val intent = Intent()

                    intent.setPackage("com.shazam.android"); // Remplacez par le package de l'application cible
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)  // On lance une nouvelle activité depuis un Composable --> New task
                    try {
                        contexte.startActivity(intent)
                    } catch (e: Exception) {
                        Log.d("ERROR", "erreur ${e.message}")
                    }
                },
        ) { Text("Lancer Shazam") }
        Button(
                onClick = {
                    try {

                        val callIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:0320123456"))

                        // Lancez l'application d'appel téléphonique
                        launcher.launch(callIntent)
                    } catch (e: ActivityNotFoundException) {
                        Log.e("Activity", "${e.message}")
                    }
                },

                ) {
            Text(text = "Appeler qqn")
        }
        // Bouton Afficher carte
        Button(
                onClick = {
                    try {
                        val uri = Uri.parse("geo:50.60859,3.1368847,17z?z=13")
                        // val uri = Uri.parse("geo:0,0?q=Université+de+Lille,+Villeneuve-d'Ascq,+France")

                        val mapIntent = Intent(Intent.ACTION_VIEW, uri)
                        launcher.launch(mapIntent)
                    } catch (e: ActivityNotFoundException) {
                        Log.e("Activity", "${e.message}")
                    }
                },

                ) {
            Text(text = "Afficher une carte")
        }
        // Bouton Afficher carte avec Google Maps
        Button(
                onClick = {
                    try {

                        val uri = Uri.parse("geo:50.60859,3.1368847,17z?z=13")
                        // val uri = Uri.parse("geo:0,0?q=Université+de+Lille,+Villeneuve-d'Ascq,+France")

                        val mapIntent = Intent(Intent.ACTION_VIEW, uri)
                        mapIntent.setPackage("com.google.android.apps.maps")
                        launcher.launch(mapIntent)

                    } catch (e: ActivityNotFoundException) {
                        Log.e("Activity", "${e.message}")
                    }
                },

                ) {
            Text(text = "Google Maps")
        }
        // Bouton Afficher page web
        Button(
                onClick = {
                    try {

                        val webIntent: Intent = Uri.parse("https://www.android.com").let { webpage ->
                            Intent(Intent.ACTION_VIEW, webpage)
                        }

                        launcher.launch(webIntent)
                    } catch (e: ActivityNotFoundException) {
                        Log.e("Activity", "${e.message}")
                    }
                },

                ) {
            Text(text = "Page web")
        }
        // Bouton Email
        Button(
                onClick = {
                    try {

                        val emailIntent: Intent = Intent(Intent.ACTION_SEND).apply {
                            // The intent does not have a URI, so declare the "text/plain" MIME type
                            type = "text/plain"
                            putExtra(Intent.EXTRA_EMAIL, arrayOf("jan@example.com")) // recipients
                            putExtra(Intent.EXTRA_SUBJECT, "Ceci est un exemple de sujet de mail")
                            putExtra(Intent.EXTRA_TEXT, "Ceci es un exemple de contenu de mail")
                            putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"))
                            // You can also attach multiple items by passing an ArrayList of Uris

                        }

                        launcher.launch(emailIntent)
                    } catch (e: ActivityNotFoundException) {
                        Log.e("Activity", "${e.message}")
                    }
                },

                ) {
            Text(text = "Email")
        }

        // Bouton Agenda
        Button(
                onClick = {
                    try {

                        val agendaIntent: Intent = // Event is on Decemnbre, 6th, 2023 -- from 13:30 to 17:45
                                Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI).apply {
                                    val beginTime: Calendar = Calendar.getInstance().apply {
                                        set(2023, 11, 6, 13, 30)
                                    }
                                    val endTime = Calendar.getInstance().apply {
                                        set(2023, 11, 6, 17, 45)
                                    }
                                    putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.timeInMillis)
                                    putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.timeInMillis)
                                    putExtra(CalendarContract.Events.TITLE, "TAC (cours et TD)")
                                    putExtra(CalendarContract.Events.EVENT_LOCATION, "P1 puis M5")
                                }

                        launcher.launch(agendaIntent)
                    } catch (e: ActivityNotFoundException) {
                        Log.e("Activity", "${e.message}")
                    }
                },
        ) {
            Text(text = "Agenda")
        }
    }
}
